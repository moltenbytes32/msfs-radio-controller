# MSFS Radio Controller
This is an open source radio controller that was built for Microsoft Flight Simulator 2020.

![finished radio controller](images/20211003_151220.jpg "Finished Radio Controller")

# Index
path | description
---- | -----------
`arduino/` | The source code and project for the arduino that powers the radio controller.
`build instructions/` | The graphical instructions for physically building the radio controller
`images/` | Images I had to reference in this file or elsewhere in the repo that didn't have another home
`win10/` | The windows 10 software that interfaces with MSFS 2020
`BOM.md` | The Bill of Materials for physically building the radio
`devlog.md` | The journal I keep as I develop on the project
`readme.md` | [recursion](./readme.md)

# Physical Build
If you want to build this for yourself, you'll want to start by obtaining everything outlined in the [bill of materials](./BOM.md). From there, [solder](./build instructions/soldering.png) the basic components together, [assemble](./build instructions/assembly.png) everything into the case, and [wire](./build instructions/wiring.png) everything together.

# Software Upload
Once you've physically built the radio controller, you'll need the software to make it work. In this case, there are two necessary parts -- the arduino code that needs to be built and uploaded to the controller, and the MSFS mod that needs to be running on your PC so it can talk to the game.

## Arduino Program
Building the arduino program is easy:
1. Clone this repo (so that you can easily pull updates in the future!)
2. Install and setup the [Arduino CLI](https://arduino.github.io/arduino-cli/0.19/getting-started/)
3. Install the arduino package for the [Sparkfun Arduino Boards](https://github.com/sparkfun/Arduino_Boards#installation-instructions)
4. Plug in the radio controller, and confirm the COM port (we'll assume `COM_X`)
5. Open a shell in ./arduino and run `$ arduino-cli compile -up COM_X`

## MSFS Mod
Once the arduino program has been uploaded, using the windows software is pretty easy -- open the software, plug in the radio controller when prompted, and then start MSFS (no worries if MSFS is already running, just make sure to plug the radio controller in _after_ starting the software).

In order to obtain the software, go to the [latest release](https://gitlab.com/moltenbytes32/msfs-radio-controller/-/releases) and download the `exe`. Easy!

If you want to build the source code from scratch, you'll need to download and install the latest MSFS SDK, as well as MSVC 2019 (with the C++ workload!), but after that you should be able to open the solution directly and build it for yourself.