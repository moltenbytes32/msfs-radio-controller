#pragma once

#include "common/Common.hpp"
#include "common/Messages.hpp"

#include <Windows.h>

#include <fstream>
#include <functional>
#include <map>
#include <set>
#include <optional>

/**
 * Provides a simple serial interface to the arduino radio board
 */
class SerialApi
{
public:

	/** The possible connection states */
	enum class ConnectionState
	{
		SEARCHING,
		ESTABLISHED,
	};

	/** Default constructor */
	SerialApi();

	/** Tries to establish a connection if not connected */
	void tryConnect();

	/** Processes pending serial data */
	void update();

	/** Returns the current connection state */
	ConnectionState getConnectionState() const;

	/** Sets the channel spacing (if connected) */
	void setChannelSpacing(std::uint32_t channelSpacing_Hz);

	/** Sets the frequency of the given radio (if connected) */
	void setFrequency(RadioID radio, std::uint32_t frequency_Hz);

	/** Sets the power state of the given radio (if connected) */
	void setPowerState(bool powerOn);

	/** The ID of a freq changed callback */
	struct FreqChangeCallbackID { std::size_t val; };
	/** 
	 * A callback that will be invoked whenever frequencies change
	 * @param radio			the radio that changed
	 * @param freq_Hz		the new selected frequency
	 */
	using FreqChangeCallback = std::function<void(RadioID radio, std::uint32_t freq_Hz)>;

	/**
	 * @param callback The function to call on change
	 * @returns the ID of the callback
	 */
	FreqChangeCallbackID registerCallback(FreqChangeCallback && callback);

	/** @param id the ID of the callback to remove */
	void deregisterCallback(FreqChangeCallbackID id);

private:
	/** The last read message header (reset when the body has been processed) */
	Messages::Header lastHeader;

	/** The set of currently active COM ports; used when trying to locate the device */
	std::set<int> activeCOMs;
	/** The current COM connection */
	HANDLE comPort;

	/** The collection of all frequency change callbacks */
	std::map<std::size_t, FreqChangeCallback> freqChangeCallbacks;
};