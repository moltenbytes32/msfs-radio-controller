#pragma once

#include "Radio.hpp"

#include "common/Common.hpp"

#include <Windows.h>
struct SIMCONNECT_RECV;

#include <cstddef>
#include <functional>
#include <map>

/**
 * Provides an abstraction over this application's use of SimConnect
 */
class SimConnectApi
{
public:
	/** The state this API's connection can be in */
	enum class ConnectionState
	{
		/** Not connected to the sim */
		Disconnected,
		/** Connected, sim not running */
		Connected,
		/** Connected, sim is running */
		Running,
	};

	/** API Initialization */
	SimConnectApi();

	/**
	 * Blocks until a connection has been received
	 * @returns the connection state at the end of this call
	 */
	ConnectionState waitForConnection();

	/**
	 * Immediatley tries to connect
	 * @returns the connection state at the end of this call
	 */
	ConnectionState tryForConnection();

	/** @returns the current connection state */
	ConnectionState getConnectionState() const;

	/** Processes sim connect events and whatnot */
	void update();

	/** Sets the radio channel spacing in-sim */
	void setChannelSpacing(RadioChannelSpacing spacing);

	/**
	 * Sets a radio frequency
	 * @param radio	The radio to set (1 or 2)
	 * @param com True for com, false for nav
	 * @param active True for active, false for standby
	 * @param freq_Hz the frequency to set
	 */
	void setRadioFrequency(RadioID radio, std::uint32_t freq_Hz);

	/** The ID for a channel spacing callback */
	struct ChannelSpacingCallbackID { std::size_t val; };
	/** The type of a channel spacing callback */
	using ChannelSpacingCallback = std::function<void(std::uint32_t, std::uint32_t)>;

	/** The ID for a frequency change callback */
	struct FreqChangeCallbackID { std::size_t val; };
	/** The type of a frequency change callback */
	using FreqChangeCallback = std::function<void(RadioID, std::uint32_t)>;

	/** The ID for a power change callback */
	struct PowerChangeCallbackID { std::size_t val; };
	/** The type of a power change callback */
	using PowerChangeCallback = std::function<void(bool)>;

	/**
	 * @param callback The callback to invoke when channel spacing changes
	 * @returns the ID of the callback to be used to remove it
	 */
	ChannelSpacingCallbackID registerCallback(ChannelSpacingCallback && callback);
	FreqChangeCallbackID registerCallback(FreqChangeCallback && callback);
	PowerChangeCallbackID registerCallback(PowerChangeCallback && callback);

	/** @param the ID of the callback to deregister */
	void deregisterCallback(ChannelSpacingCallbackID callback);
	void deregisterCallback(FreqChangeCallbackID callback);
	void deregisterCallback(PowerChangeCallbackID callback);

private:
	/** Handles connection state changes */
	void setConnectionState(ConnectionState newState);
	/** Handles simconnect events */
	void eventHandler(SIMCONNECT_RECV * data);

	/** The current connection state */
	ConnectionState connectionState;
	/** The handle to simconnect */
	HANDLE hSimConnect;

	/** The channel spacing callbacks */
	std::map<std::size_t, ChannelSpacingCallback> channelSpacingCallbacks;
	/** The frequency change callbacks */
	std::map<std::size_t, FreqChangeCallback> freqChangeCallbacks;
	/** The power change callbacks */
	std::map<std::size_t, PowerChangeCallback> powerChangeCallbacks;

	/** Stores the last known frequency for each radio to prevent duplicate message processing */
	std::map<RadioID, int> pendingFreqWrites;
};
