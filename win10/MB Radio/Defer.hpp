#pragma once

#include <utility>

template<typename Callback>
class Defer
{
public:
	Defer(Callback && callback_)
		:callback(std::move(callback_))
		,invoke(true)
	{

	}

	~Defer()
	{
		if (std::exchange(invoke, false)) callback();
	}

	void cancel()
	{
		invoke = false;
	}

private:
	Callback callback;
	bool invoke;
};