#include "SerialApi.hpp"

#include "Defer.hpp"

#include <WinBase.h>
#include <Windows.h>
#undef min
#undef max

#include <algorithm>
#include <limits>
#include <iostream>
#include <string>
#include <vector>

/** The message header structure */
struct MessageHeader
{
	static constexpr std::uint8_t MAGIC_VAL = 0xf0;
	std::uint8_t magic = MAGIC_VAL;
	
	enum class Radios : std::uint8_t
	{
		BEGIN = 1,
		Radio1 = 1,
		Radio2 = 2,
		END
	} radio;

	enum class MessageIDs : std::uint8_t
	{
		BEGIN = 1,
		SetChannelSpacing = 1,
		END,
	} messageID;
};

/** Scans Windows for the currently active COM ports */
static std::set<int> getActiveComPorts()
{
	std::set<int> ports;

	for (int i = 0; i < 255; ++i)
	{
		std::wstring portName = L"COM" + std::to_wstring(i);
		wchar_t buffer;
		if (QueryDosDevice(portName.c_str(), &buffer, 1) ||
			::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
		{
			ports.insert(i);
		}
	}

	return ports;
}

/** Returns the number of bytes waiting at a com port */
static DWORD getAvailableBytes(HANDLE comPort)
{
	DWORD errors;
	COMSTAT stat;
	ClearCommError(comPort, &errors, &stat);
	return stat.cbInQue;
}

/** Prints the last error */
static void printError()
{
	DWORD errCode = GetLastError();
	if (!errCode) return;

	LPWSTR messageBuffer;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		errCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		reinterpret_cast<LPWSTR>(&messageBuffer),
		0,
		NULL
	);

	std::wcout << '\t' << messageBuffer << '\n';
	LocalFree(messageBuffer);
}

/** Sends the given header and body */
template<typename BodyT>
void sendMessage(HANDLE comPort, Messages::Header const & header, BodyT const & body)
{
	DWORD bytesWritten;
	WriteFile(
		comPort,
		&header,
		sizeof(header),
		&bytesWritten,
		NULL
	);
	WriteFile(
		comPort,
		&body,
		sizeof(body),
		&bytesWritten,
		NULL
	);

	if (GetLastError() != 0)
	{
		std::cerr << "Serial: Write Failure!\n";
		printError();
	}
}

SerialApi::SerialApi()
	:activeCOMs(getActiveComPorts())
	,comPort(INVALID_HANDLE_VALUE)
{
	std::cerr << "Serial: Trying to find radio, please plug it in now\n";
}

void SerialApi::tryConnect()
{
	if (getConnectionState() == ConnectionState::ESTABLISHED) return;

	std::set<int> newActiveCOMs = getActiveComPorts();
	std::set<int> newCOMs;

	for (int port : newActiveCOMs)
	{
		if (!activeCOMs.contains(port))
		{
			newCOMs.insert(port);
		}
	}
	activeCOMs = std::move(newActiveCOMs);

	if (newCOMs.size() > 1)
	{
		std::cout << "Serial: Multiple new devices detected.\
Serial: Please (re)plug in the radio.";
	}
	else if (newCOMs.size() == 1)
	{
		Defer errorOut([this]()
		{
			printError();
			comPort = INVALID_HANDLE_VALUE;
		});

		std::wstring comName = L"\\\\.\\COM" + std::to_wstring(*newCOMs.begin());
		std::wcout << L"Serial: Radio found (" << comName << L")\n";

		comPort = CreateFile(
			comName.c_str(),
			GENERIC_READ | GENERIC_WRITE,
			NULL,
			NULL,
			OPEN_EXISTING,
			NULL,
			NULL
		);

		if (comPort == INVALID_HANDLE_VALUE)
		{
			std::cerr << "Serial: Failed to open COM port!\n";
			return;
		}

		DCB dcb = { 0 };
		dcb.DCBlength = sizeof(dcb);

		if (!GetCommState(comPort, &dcb))
		{
			std::cerr << "Serial: Failed to get COM state!\n";
			return;
		}

		dcb.BaudRate = CBR_9600;
		if (!SetCommState(comPort, &dcb))
		{
			std::cerr << "Serial: Failed to set COM state!\n";
			return;
		}

		COMMTIMEOUTS timeouts;
		timeouts.ReadIntervalTimeout = MAXDWORD;
		timeouts.ReadTotalTimeoutMultiplier = 0;
		timeouts.ReadTotalTimeoutConstant = 0;
		timeouts.WriteTotalTimeoutMultiplier = 0;
		timeouts.WriteTotalTimeoutConstant = 0;

		if (!SetCommTimeouts(comPort, &timeouts))
		{
			std::cerr << "Serial: Failed to set COM timeout!\n";
			return;
		}

		std::cout << "Serial: Radio Connected\n";
		errorOut.cancel();
	}
}

void SerialApi::update()
{
	tryConnect();

	if (getConnectionState() == ConnectionState::ESTABLISHED)
	{
		while (!lastHeader && getAvailableBytes(comPort) >= sizeof(MessageHeader))
		{
			std::uint8_t magic;
			DWORD bytesRead;
			ReadFile(
				comPort,
				&magic,
				sizeof(magic),
				&bytesRead,
				NULL
			);

			if (magic == Messages::Header::MAGIC_VAL)
			{
				static_assert(offsetof(Messages::Header, radio) == 1);
				lastHeader.magic = Messages::Header::MAGIC_VAL;
				ReadFile(
					comPort,
					&lastHeader.radio,
					sizeof(Messages::Header) - 1,
					&bytesRead,
					NULL
				);
			}
		}

		if (lastHeader)
		{
			switch (lastHeader.messageID)
			{
			case Messages::Header::MessageIDs::SetFrequency:
				if (getAvailableBytes(comPort) >= sizeof(Messages::FreqencyBody))
				{
					Messages::FreqencyBody body;
					DWORD bytesRead;
					ReadFile(
						comPort,
						&body,
						sizeof(body),
						&bytesRead,
						NULL
					);

					// round to a multiple of 5k
					body.freq_Hz -= body.freq_Hz % 5'000;

					for (auto & [key, callback] : freqChangeCallbacks)
					{
						callback(lastHeader.radio, body.freq_Hz);
					}

					lastHeader = Messages::Header();
				}
				break;

			case Messages::Header::MessageIDs::Debug:
				while (getAvailableBytes(comPort) && lastHeader)
				{
					char message;
					DWORD bytesRead;	// seems like overkill
					ReadFile(
						comPort,
						&message,
						sizeof(char),
						&bytesRead,
						NULL
					);

					if (message != '\0') std::cout << message;
					else
					{
						std::cout << '\n';
						lastHeader = Messages::Header();
					}
				}

			default:
				lastHeader = Messages::Header();
			}
		}
	}
}

SerialApi::ConnectionState SerialApi::getConnectionState() const
{
	return comPort == INVALID_HANDLE_VALUE ? ConnectionState::SEARCHING : ConnectionState::ESTABLISHED;
}

void SerialApi::setChannelSpacing(std::uint32_t channelSpacing_Hz)
{
	if (getConnectionState() == ConnectionState::ESTABLISHED)
	{
		Messages::Header header;
		header.magic = Messages::Header::MAGIC_VAL;
		header.radio.index = 0;
		header.radio.com = true;
		header.messageID = Messages::Header::MessageIDs::SetChannelSpacing;

		Messages::ChannelSpaceBody body;
		body.spacing_Hz = channelSpacing_Hz;

		DWORD bytesWritten;
		WriteFile(
			comPort,
			&header,
			sizeof(header),
			&bytesWritten,
			NULL
		);
		WriteFile(
			comPort,
			&body,
			sizeof(body),
			&bytesWritten,
			NULL
		);

		if (GetLastError() != 0)
		{
			std::cerr << "Serial: Write Failure!\n";
			printError();
		}
	}
}

void SerialApi::setFrequency(RadioID radio, std::uint32_t frequency_Hz)
{
	if (getConnectionState() == ConnectionState::ESTABLISHED)
	{
		Messages::Header header;
		header.magic = Messages::Header::MAGIC_VAL;
		header.radio = radio;
		header.messageID = Messages::Header::MessageIDs::SetFrequency;

		Messages::FreqencyBody body;
		body.freq_Hz = frequency_Hz;

		DWORD bytesWritten;
		WriteFile(
			comPort,
			&header,
			sizeof(header),
			&bytesWritten,
			NULL
		);
		WriteFile(
			comPort,
			&body,
			sizeof(body),
			&bytesWritten,
			NULL
		);

		if (GetLastError() != 0)
		{
			std::cerr << "Serial: Write Failure!\n";
			printError();
		}
	}
}

void SerialApi::setPowerState(bool powerState)
{
	if (getConnectionState() == ConnectionState::ESTABLISHED)
	{
		Messages::Header header;
		header.magic = Messages::Header::MAGIC_VAL;
		header.radio = { .active = 0, .com = 0, .index = 0 };
		header.messageID = Messages::Header::MessageIDs::Power;

		Messages::PowerBody body;
		body.on = powerState;

		sendMessage(comPort, header, body);
	}
}

SerialApi::FreqChangeCallbackID SerialApi::registerCallback(FreqChangeCallback && callback)
{
	static std::size_t id = 0;
	++id;
	freqChangeCallbacks.emplace(id, std::move(callback));
	return { id };
}

void SerialApi::deregisterCallback(FreqChangeCallbackID id)
{
	freqChangeCallbacks.erase(id.val);
}