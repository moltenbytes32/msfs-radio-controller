#include "SimConnectApi.hpp"

#include <SimConnect.h>

#include <iostream>
#include <optional>

struct ChannelSpacing
{
	std::uint32_t com1spacing;
	std::uint32_t com2spacing;
};

struct FrequencyTag
{
	std::int32_t id;
	std::int32_t freq;

	enum IDs
	{
		COM_1_ACTIVE,
		COM_1_STANDBY,
		NAV_1_ACTIVE,
		NAV_1_STANDBY,
	};
};

struct PowerAvailable
{
	std::uint32_t avionicsOn;
};

enum class Definitions
{
	ChannelSpacing,
	FrequencyTag,
	PowerAvailable,
};

enum class Events
{
	SimStarted,
	SimStopped,
	SetCom1Active,
	SetCom1Standby,
	SetNav1Active,
	SetNav1Standby,
};

enum class NotificationGroups
{
	Default,
};

enum class Requests
{
	SimState,
	ChannelSpacing,
	FrequencyTag,
	PowerAvailable,
};

SimConnectApi::SimConnectApi()
	:connectionState(ConnectionState::Disconnected)
	,hSimConnect(NULL)
{
	std::cout << "MSFS: Awaiting Connection...\n";
}

SimConnectApi::ConnectionState SimConnectApi::waitForConnection()
{
	while (tryForConnection() == ConnectionState::Disconnected);
	return connectionState;
}

SimConnectApi::ConnectionState SimConnectApi::tryForConnection()
{
	if (connectionState >= ConnectionState::Connected) return connectionState;

	if (SUCCEEDED(SimConnect_Open(&hSimConnect, "MoltenBytes' Radio Interface", NULL, 0, 0, 0)))
	{
		setConnectionState(ConnectionState::Connected);

		// request future state changes
		SimConnect_SubscribeToSystemEvent(hSimConnect, static_cast<int>(Events::SimStarted), "SimStart");
		SimConnect_SubscribeToSystemEvent(hSimConnect, static_cast<int>(Events::SimStopped), "SimStop");

		// request the current state
		SimConnect_RequestSystemState(hSimConnect, static_cast<int>(Requests::SimState), "Sim");

		// setup radio parameter events
		SimConnect_AddToDataDefinition(
			hSimConnect,
			static_cast<int>(Definitions::ChannelSpacing),
			"COM SPACING MODE:1",
			NULL,
			SIMCONNECT_DATATYPE_INT32
		);
		SimConnect_AddToDataDefinition(
			hSimConnect,
			static_cast<int>(Definitions::ChannelSpacing),
			"COM SPACING MODE:2",
			NULL,
			SIMCONNECT_DATATYPE_INT32
		);

		SimConnect_RequestDataOnSimObject(
			hSimConnect,
			static_cast<int>(Requests::ChannelSpacing),
			static_cast<int>(Definitions::ChannelSpacing),
			SIMCONNECT_OBJECT_ID_USER,
			SIMCONNECT_PERIOD_SIM_FRAME,
			SIMCONNECT_DATA_REQUEST_FLAG_CHANGED
		);

		/** Setup frequency listeners */
		SimConnect_AddToDataDefinition(hSimConnect, static_cast<int>(Definitions::FrequencyTag), "COM ACTIVE FREQUENCY:1", "Hz", SIMCONNECT_DATATYPE_INT32, 0, static_cast<int>(FrequencyTag::COM_1_ACTIVE));
		SimConnect_AddToDataDefinition(hSimConnect, static_cast<int>(Definitions::FrequencyTag), "COM STANDBY FREQUENCY:1", "Hz", SIMCONNECT_DATATYPE_INT32, 0, static_cast<int>(FrequencyTag::COM_1_STANDBY));
		SimConnect_AddToDataDefinition(hSimConnect, static_cast<int>(Definitions::FrequencyTag), "NAV ACTIVE FREQUENCY:1", "Hz", SIMCONNECT_DATATYPE_INT32, 0, static_cast<int>(FrequencyTag::NAV_1_ACTIVE));
		SimConnect_AddToDataDefinition(hSimConnect, static_cast<int>(Definitions::FrequencyTag), "NAV STANDBY FREQUENCY:1", "Hz", SIMCONNECT_DATATYPE_INT32, 0, static_cast<int>(FrequencyTag::NAV_1_STANDBY));

		SimConnect_RequestDataOnSimObject(
			hSimConnect,
			static_cast<int>(Requests::FrequencyTag),
			static_cast<int>(Definitions::FrequencyTag),
			SIMCONNECT_OBJECT_ID_USER,
			SIMCONNECT_PERIOD_SIM_FRAME,
			SIMCONNECT_DATA_REQUEST_FLAG_CHANGED | SIMCONNECT_DATA_REQUEST_FLAG_TAGGED
		);

		/** Setup frequency change events */
		SimConnect_MapClientEventToSimEvent(hSimConnect, static_cast<int>(Events::SetCom1Active), "COM_RADIO_SET_HZ");
		SimConnect_AddClientEventToNotificationGroup(hSimConnect, static_cast<int>(NotificationGroups::Default), static_cast<int>(Events::SetCom1Active), false);
		SimConnect_MapClientEventToSimEvent(hSimConnect, static_cast<int>(Events::SetCom1Standby), "COM_STBY_RADIO_SET_HZ");
		SimConnect_AddClientEventToNotificationGroup(hSimConnect, static_cast<int>(NotificationGroups::Default), static_cast<int>(Events::SetCom1Standby), false);
		SimConnect_MapClientEventToSimEvent(hSimConnect, static_cast<int>(Events::SetNav1Active), "NAV1_RADIO_SET_HZ");
		SimConnect_AddClientEventToNotificationGroup(hSimConnect, static_cast<int>(NotificationGroups::Default), static_cast<int>(Events::SetNav1Active), false);
		SimConnect_MapClientEventToSimEvent(hSimConnect, static_cast<int>(Events::SetNav1Standby), "NAV1_STBY_SET_HZ");
		SimConnect_AddClientEventToNotificationGroup(hSimConnect, static_cast<int>(NotificationGroups::Default), static_cast<int>(Events::SetNav1Standby), false);

		SimConnect_SetNotificationGroupPriority(hSimConnect, static_cast<int>(NotificationGroups::Default), SIMCONNECT_GROUP_PRIORITY_HIGHEST);
	
		/** Setup power listeners */
		SimConnect_AddToDataDefinition(hSimConnect, static_cast<int>(Definitions::PowerAvailable), "CIRCUIT AVIONICS ON", "", SIMCONNECT_DATATYPE_INT32);
		SimConnect_RequestDataOnSimObject(
			hSimConnect,
			static_cast<int>(Requests::PowerAvailable),
			static_cast<int>(Definitions::PowerAvailable),
			SIMCONNECT_OBJECT_ID_USER,
			SIMCONNECT_PERIOD_SIM_FRAME,
			SIMCONNECT_DATA_REQUEST_FLAG_CHANGED
		);
	}

	return connectionState;
}

SimConnectApi::ConnectionState SimConnectApi::getConnectionState() const
{
	return connectionState;
}

void SimConnectApi::update()
{
	if (tryForConnection() < ConnectionState::Connected) return;
	SimConnect_CallDispatch(hSimConnect, [](SIMCONNECT_RECV* data, DWORD, void * context)
		{
			reinterpret_cast<SimConnectApi*>(context)->eventHandler(data);
		}, this);
}

void SimConnectApi::setChannelSpacing(RadioChannelSpacing spacing)
{
	if (connectionState < ConnectionState::Running) return;

	ChannelSpacing simSpacing
	{
		static_cast<std::uint32_t>(spacing.val),
		static_cast<std::uint32_t>(spacing.val),
	};

	HRESULT hr = SimConnect_SetDataOnSimObject(
		hSimConnect,
		static_cast<int>(Definitions::ChannelSpacing),
		SIMCONNECT_OBJECT_ID_USER,
		NULL,
		1,
		sizeof(simSpacing),
		&simSpacing
	);
}

void SimConnectApi::setRadioFrequency(RadioID radio, std::uint32_t freq_Hz)
{
	static Events const events [] = {
		Events::SetCom1Active,
		Events::SetCom1Standby,
		Events::SetNav1Active,
		Events::SetNav1Standby,
	};

	auto event = events[(radio.index * 4) + (!radio.com * 2) + (!radio.active * 1)];

	SimConnect_TransmitClientEvent(
		hSimConnect,
		SIMCONNECT_OBJECT_ID_USER,
		static_cast<int>(event),
		freq_Hz,
		SIMCONNECT_GROUP_PRIORITY_HIGHEST,
		SIMCONNECT_EVENT_FLAG_GROUPID_IS_PRIORITY
	);

	pendingFreqWrites[radio]++;
}

SimConnectApi::ChannelSpacingCallbackID SimConnectApi::registerCallback(ChannelSpacingCallback && callback)
{
	static std::size_t callbackID = 0;
	++callbackID;
	channelSpacingCallbacks.emplace(callbackID, std::move(callback));
	return { callbackID };
}

SimConnectApi::FreqChangeCallbackID SimConnectApi::registerCallback(FreqChangeCallback && callback)
{
	static std::size_t callbackID = 0;
	++callbackID;
	freqChangeCallbacks.emplace(callbackID, std::move(callback));
	return { callbackID };
}

SimConnectApi::PowerChangeCallbackID SimConnectApi::registerCallback(PowerChangeCallback && callback)
{
	static std::size_t callbackID = 0;
	++callbackID;
	powerChangeCallbacks.emplace(callbackID, std::move(callback));
	return { callbackID };
}

void SimConnectApi::deregisterCallback(ChannelSpacingCallbackID id)
{
	channelSpacingCallbacks.erase(id.val);
}

void SimConnectApi::deregisterCallback(FreqChangeCallbackID id)
{
	freqChangeCallbacks.erase(id.val);
}

void SimConnectApi::deregisterCallback(PowerChangeCallbackID id)
{
	powerChangeCallbacks.erase(id.val);
}

void SimConnectApi::setConnectionState(ConnectionState newState)
{
	connectionState = newState;
	switch (newState)
	{
	case ConnectionState::Disconnected:
		std::cout << "MSFS: Connection Lost...\n";
		break;

	case ConnectionState::Connected:
		std::cout << "MSFS: Connected (Sim Cold)\n";
		break;

	case ConnectionState::Running:
		std::cout << "MSFS: Connected (Sim Hot)\n";
		break;
	}
}

void SimConnectApi::eventHandler(SIMCONNECT_RECV * data)
{
	switch (data->dwID)
	{
		case SIMCONNECT_RECV_ID_EVENT:
		{
			auto * event = reinterpret_cast<SIMCONNECT_RECV_EVENT*>(data);
			switch (static_cast<Events>(event->uEventID))
			{
			case Events::SimStarted:
				setConnectionState(ConnectionState::Running);
				break;

			case Events::SimStopped:
				setConnectionState(ConnectionState::Connected);
				break;
			}

			break;
		}

		case SIMCONNECT_RECV_ID_SIMOBJECT_DATA:
		{
			auto * objData = reinterpret_cast<SIMCONNECT_RECV_SIMOBJECT_DATA *>(data);
			switch (static_cast<Requests>(objData->dwRequestID))
			{
			case Requests::ChannelSpacing:
				{
					auto * spacing = reinterpret_cast<ChannelSpacing *>(&objData->dwData);
					spacing->com1spacing = spacing->com1spacing ? 6'250 : 25'000;
					spacing->com2spacing = spacing->com2spacing ? 6'250 : 25'000;

					for (auto & [id, callback] : channelSpacingCallbacks)
					{
						callback(spacing->com1spacing, spacing->com2spacing);
					}
				}
				break;

			case Requests::FrequencyTag:
				{
					auto tags = reinterpret_cast<FrequencyTag *>(&objData->dwData);
					for (DWORD i = 0; i < objData->dwDefineCount; ++i)
					{
						std::optional<RadioID> id;
						switch (tags[i].id)
						{
						case FrequencyTag::COM_1_ACTIVE: id = { .active = true, .com = true, .index = 0 }; break;
						case FrequencyTag::COM_1_STANDBY: id = { .active = false, .com = true, .index = 0 }; break;
						case FrequencyTag::NAV_1_ACTIVE: id = { .active = true, .com = false , .index = 0 }; break;
						case FrequencyTag::NAV_1_STANDBY: id = { .active = false, .com = false, .index = 0 }; break;
						}

						if (id)
						{
							auto writesIt = pendingFreqWrites.insert({ *id, 0 }).first;
							if (writesIt->second == 0)
							{
								for (auto & [key, callback] : freqChangeCallbacks) callback(*id, tags[i].freq);
							}
							else
							{
								--writesIt->second;
							}
						}
					}
				}
				break;

			case Requests::PowerAvailable:
				{
					auto * powerAvailable = reinterpret_cast<PowerAvailable *>(&objData->dwData);
					bool b = true;
					for (auto & [id, callback] : powerChangeCallbacks) callback(powerAvailable->avionicsOn);
				}
				break;
			}
		}

		case SIMCONNECT_RECV_ID_SYSTEM_STATE:
		{
			auto * state = reinterpret_cast<SIMCONNECT_RECV_SYSTEM_STATE*>(data);
			if (static_cast<Requests>(state->dwRequestID) == Requests::SimState)
			{
				if (state->dwInteger == 0) setConnectionState(ConnectionState::Connected);
				else if (state->dwInteger == 1) setConnectionState(ConnectionState::Running);
			}
			break;
		}

		case SIMCONNECT_RECV_ID_QUIT:
			setConnectionState(ConnectionState::Disconnected);
			break;
	}
}