#pragma once

#include <cstdint>

/** The data structure for passing around channel spacing */
struct RadioChannelSpacing
{
	enum Value : std::uint8_t
	{
		US,
		EU
	} val = US;

	static inline RadioChannelSpacing fromHz(std::uint32_t spacing_Hz)
	{
		if (spacing_Hz == 25000) return { US };
		return { EU };
	}
};