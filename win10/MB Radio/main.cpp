#include "Radio.hpp"
#include "SerialApi.hpp"
#include "SimConnectApi.hpp"

#include "common/Common.hpp"
#include "common/Version.hpp"

#include <chrono>
#include <iostream>
#include <thread>

int main()
{
	std::cout << "MoltenBytes Radio Driver\n";
	std::cout << "  Version " << versionStr << "\n\n";

	SerialApi serial;

	while (serial.getConnectionState() < SerialApi::ConnectionState::ESTABLISHED) serial.tryConnect();

	SimConnectApi simConnect;

	serial.registerCallback([&serial, &simConnect](RadioID radio, std::uint32_t freq_Hz)
		{
			simConnect.setRadioFrequency(radio, freq_Hz);
		});

	simConnect.registerCallback([&serial, &simConnect](std::uint32_t radio1, std::uint32_t radio2)
		{
			serial.setChannelSpacing(radio1);
		});
	simConnect.registerCallback([&serial, &simConnect](RadioID radio, std::uint32_t freq_Hz)
		{
			serial.setFrequency(radio, freq_Hz);
		});
	simConnect.registerCallback([&serial, &simConnect](bool powerOn)
		{
			serial.setPowerState(powerOn);
		});

	while (true)
	{
		serial.update();
		simConnect.update();
		simConnect.setChannelSpacing(RadioChannelSpacing{ RadioChannelSpacing::US });

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}
