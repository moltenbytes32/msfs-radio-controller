# Bill of Materials

Part | Unit Price | Qty | Price
---- | ---------- | --- | -----
[3.3v Pro Micro](https://www.sparkfun.com/products/12587) |  $ 17.95 | 1 | $ 17.95
[Sparkfun 16x2 SerLCD](https://www.sparkfun.com/products/16397) | $ 19.95 | 1 | $ 19.95
[Propwash Sim Dual Encoder Kit](https://www.propwashsim.com/store/dual-encoder-kit) | $ 12.95 | 1 | $ 12.95
[MBSS Pico Protoboard](https://www.amazon.com/MBSS-Solderable-Breadboard-Proto-Board/dp/B082PVGYX3) | $ 7.99 | 1/10 | $ 0.80
[MBSS Nano Protoboard](https://www.amazon.com/MBSS-Solderable-Breadboard-Proto-Board/dp/B082PV1V6S) | $ 8.99 | 1/10 | $ 0.90
[Male Headers](https://www.amazon.com/MCIGICM-Header-2-45mm-Arduino-Connector/dp/B07PKKY8BX) | $ 4.99 | 52/400 | $ 0.65
[10cm Female Jumpers](https://www.amazon.com/EDGELEC-Optional-Breadboard-Assorted-Multicolored/dp/B07GD312VG) | $ 6.98 | 14/120 | $ 0.81
[15cm Female Jumpers](https://www.amazon.com/EDGELEC-Optional-Breadboard-Assorted-Multicolored/dp/B07GCZVCGS) | $ 7.49 | 4/120 | $ 0.25
[3D Printed Case](https://www.thingiverse.com/thing:5059552)† | $ 0.66 | 1 | $ 0.66
M2*12 Bolt‡ | $ 0.024 | 4 | $ 0.10
M2*12 Nut‡ | $ 0.024 | 4 | $ 0.10
M3*12 Bolt‡ | $ 0.024 | 2 | $ 0.05
M3*12 Nut‡ | $ 0.024 | 2 | $ 0.05
M3*12 Washer‡ | $ 0.024 | 1 | $ 0.02
M3*8 Bolt‡ | $ 0.024 | 1 | $ 0.02
M3*8 Nut‡ | $ 0.024 | 1 | $ 0.02
Total | | | $ 55.28

_Note †_ This is how much the filament cost me to 3D print the case. Obviously a 3D printiner will run you more than 30 cents.

_Note ‡_ The bolts, washers, and nuts are usually bought in a kit. They are quite inexpensive, but is hard to calculate. [This](https://www.amazon.com/VIGRUE-Socket-Button-Washers-Assortment/dp/B07MN9X2KN) is the kit I used; it has 1080 pieces for $26.99, which naively comes out to $0.024 per part, but this assumes that everything in the kit is equally priced.

# Kits
Because a lot of these parts are used in quantities less than order sizes, are part of larger kits, or are 3D printed, I offer a few kits that might come in handy. Simply reach out to me on Discord, Twitter, etc. This also helps fund my channel and future projects :)

## 3D Printed ($10)
_The necessary 3D printed parts_

Includes the necessary quantity of the following:
- 3D Printed Case

## Hardware ($10)
_All the nuts and bolts you need_

Includes the necessary quantity of the following, plus 1 extra:
- M2*12 Bolt
- M2*12 Nut
- M3*12 Bolt
- M3*12 Nut
- M3*12 Washer
- M3*8 Bolt
- M3*8 Nut

## Consumables ($15)
_All of the parts that were part of kits or were fractional counts_

Includes the necessary quantity of the following, plus 1 extra:
- M2*12 Bolt
- M2*12 Nut
- M3*12 Bolt
- M3*12 Nut
- M3*12 Washer
- M3*8 Bolt
- M3*8 Nut

Includes the necessary quantity of the following:
- MBSS Pico Protoboard
- MBSS Nano Protoboard
- Male Headers (2x12)
- 10cm Female Jumpers
- 15cm Female Jumpers

## Complete Kit ($75)
_The entire BOM, plus a few extras!_

Includes the necessary quantity of the following, plus 1 extra:
- M2*12 Bolt
- M2*12 Nut
- M3*12 Bolt
- M3*12 Nut
- M3*12 Washer
- M3*8 Bolt
- M3*8 Nut

Includes the necessary quantity of the following:
- 3.3v Pro Micro
- Sparkfun 16x2 SerLCD
- Propwash Sim Dual Encoder Kit
- MBSS Pico Protoboard
- MBSS Nano Protoboard
- Male Headers (2x12)
- 10cm Female Jumpers
- 15cm Female Jumpers
- 3D Printed Case