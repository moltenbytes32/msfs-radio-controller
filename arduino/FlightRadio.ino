#include "Button.hpp"
#include "ComNav.hpp"
#include "Display.hpp"
#include "Frequency.hpp"
#include "SerLink.hpp"
#include "Settings.hpp"
#include "SettingsView.hpp"

#include "debug.hpp"

// Raw hardware stuff
Display display1(0x72);
Rotary outerRotary1(1, 0);
Rotary innerRotary1(5, 4);
Button freqSwapBtn(16, true);
Button rotary1Btn(10, true);

// Serial link communications
SerLink serLink;

// The settings to use
Settings settings;
// The abstracted com/nav radio stack
ComNav radios({ false, false, 0 }, display1, settings, outerRotary1, innerRotary1, rotary1Btn, freqSwapBtn);
// The settings view
SettingsView settingsView(display1, settings, outerRotary1, innerRotary1);

// True iff we're currently displaying settings
bool showSettings = false;

// The length of rotary1Btn press-and-hold to show the settings menu
static constexpr unsigned long SETTINGS_MENU_DELAY = 3000;

void setup() {
  delay(1000);

  display1.setup();
  freqSwapBtn.setup();
  rotary1Btn.setup();
  outerRotary1.setup();
  innerRotary1.setup();

  display1.setBacklightColorSync(settings.getDisplayColor());
  display1.printSync("Written By      MoltenBytes");
  display1.sendSync(Display::Command::disableSplash);
  delay(2000);   // vanity

  serLink.setup(9600);
}

void loop() {
  serLink.poll();

  if (optional<bool> newPowerState = serLink.popPowerState())
  {
    settings.setPowerOn(*newPowerState.get_value());

    Display::Control dispControl = display1.getControl();
    dispControl.power = settings.isPowerOn();
    display1.sendSync(dispControl);

    display1.setBacklightColorSync(dispControl.power ? settings.getDisplayColor() : DisplayColor{ 0, 0, 0 });
  }

  if (uint32_t spacing = serLink.popLatestChannelSpacing())
  {
    settings.setRadioChannelSpacing(static_cast<Settings::RadioSpacing>(spacing));
  }

  if (!settings.isPowerOn()) return;

  freqSwapBtn.poll();
  rotary1Btn.poll();
  outerRotary1.poll();
  innerRotary1.poll();

  if (rotary1Btn.pressDuration() >= SETTINGS_MENU_DELAY)
  {
    rotary1Btn.cancelPress();
    showSettings = true;
  }
  if (freqSwapBtn.peek() && showSettings)
  {
    freqSwapBtn.pop();
    showSettings = false;
  }

  if (showSettings)
  {
    radios.hide();
    settingsView.show();
    settingsView.update();
  }
  else
  {
    settingsView.hide();
    radios.show();
    radios.update();
  }
}
