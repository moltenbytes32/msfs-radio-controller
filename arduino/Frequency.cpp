#include "Frequency.hpp"
#include "utility.hpp"

#include <Arduino.h>

using MHz_T = uint8_t;
using Hz_T = uint32_t;
/** Hz to MHz */
static constexpr MHz_T Hz_MHz(Hz_T hz) { return hz / 1000000; }
/** MHz to Hz */
static constexpr Hz_T MHz_Hz(MHz_T mhz ) { return mhz * 1000000; }

Frequency::Frequency(uint8_t min_mhz, uint8_t max_mhz)
  :Frequency(min_mhz, max_mhz, min_mhz, 0)
{

}

Frequency::Frequency(uint8_t min_mhz, uint8_t max_mhz, uint8_t m, uint32_t h)
  :bounds({
    min_mhz,
    max_mhz,
  })
{
  set_MHz(m);
  set_Hz(h);
}

String Frequency::toString(bool longPrecision) const
{
  uint8_t const mhz = get_MHz();
  uint16_t khz = get_Hz() / 1000;
  
  /* round khz down to the closest 5 */
  {
    int16_t khzRound5 = khz % 5;
    khz += -khzRound5;
  }

  char str [8] = {};
  str[0] = (mhz / 100) % 10 + '0';
  str[1] = (mhz /  10) % 10 + '0';
  str[2] = (mhz /   1) % 10 + '0';
  str[3] = '.';
  str[4] = (khz / 100) % 10 + '0';
  str[5] = (khz /  10) % 10 + '0';
  str[6] = (khz /   1) % 10 + '0';
  if (!longPrecision) str[6] = '\0';
  str[7] = '\0';
  return str;
}

void Frequency::set_MHz(uint8_t val_MHz)
{
  MHz_T MHz = MHz_Hz(freq_Hz);
  MHz = clamp(val_MHz, bounds.lower_MHz, bounds.upper_MHz);
  freq_Hz = get_Hz() + MHz_Hz(MHz);
}

void Frequency::offset_MHz(int8_t offset_MHz)
{
  set_MHz(Hz_MHz(freq_Hz) + offset_MHz);
}

uint8_t Frequency::get_MHz() const
{
  return Hz_MHz(freq_Hz);
}

void Frequency::set_Hz(uint32_t val_Hz)
{
  freq_Hz = MHz_Hz(get_MHz()) + (val_Hz % MHz_Hz(1));
}

void Frequency::offset_Hz(int32_t offset_Hz)
{
  set_Hz(freq_Hz + offset_Hz);
}

uint32_t Frequency::get_Hz() const
{
  return freq_Hz % MHz_Hz(1);
}

void Frequency::setFull_Hz(uint32_t val_Hz)
{
  set_MHz(Hz_MHz(val_Hz));
  set_Hz(val_Hz);
}

uint32_t Frequency::getFull_Hz() const
{
  return freq_Hz;
}

void Frequency::swap(Frequency & other)
{
  ::swap(bounds, other.bounds);
  ::swap(freq_Hz, other.freq_Hz);
}