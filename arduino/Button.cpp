#include "Button.hpp"

#include "utility.hpp"

#include <Arduino.h>

Button::Button(uint8_t pin_, bool pullup_)
  :pin(pin_)
  ,isPullup(pullup_)
  ,thisPressTime_ms(0)
  ,state(false)
  ,invalidatePress(false)
{

}

void Button::setup()
{
  pinMode(pin, isPullup ? INPUT_PULLUP : INPUT);
}

void Button::poll()
{
  if (digitalRead(pin) == !isPullup)
  {
    if (thisPressTime_ms == 0) thisPressTime_ms = millis();
  }
  else 
  {
    invalidatePress = false;
    if (exchange<unsigned long>(thisPressTime_ms, 0) != 0)
    {
      state = true;
      return;
    }
  }
  state = false;
}

bool Button::peek() const
{
  return state;
}

bool Button::pop()
{
  return exchange(state, false);
}

unsigned long Button::pressDuration() const
{
  return thisPressTime_ms != 0 ? millis() - thisPressTime_ms : 0;
}

void Button::cancelPress()
{
  invalidatePress = true;
  thisPressTime_ms = 0;
}