#include "ComNav.hpp"

#include "Button.hpp"
#include "Display.hpp"
#include "Frequency.hpp"
#include "Rotary.hpp"
#include "Settings.hpp"

#include "utility.hpp"

#include <Arduino.h>

ComNav::ComNav(RadioID radioID_, Display & display_, Settings const & settings_,
               Rotary & whole, Rotary & frac,
               Button & radioSwap_, Button & freqSwap_)
  :display(display_)
  ,wholeRotary(whole)
  ,fracRotary(frac)
  ,settings(settings_)
  ,settingsV(0) // versions start at 1, this ensures the first update succeeds
  ,radioSwapBtn(radioSwap_)
  ,freqSwapBtn(freqSwap_)
  ,radios({
    .com = Radio({
      true,
      true,
      radioID_.index,
    }, Frequency(118, 136), whole, frac, 0), // channel spacing set by settings
    .nav = Radio({
      true,
      false,
      radioID_.index,
    }, Frequency(108, 117), whole, frac, 50000),
    .comActive = true,
  })
  ,visible(false)
  ,drawDirty(true)
{
  updateSettings();
}

void ComNav::update()
{
  if (radioSwapBtn.pop())
  {
    radios.comActive = !radios.comActive;

    drawCursor();
    drawDirty = true;
  }

  Radio & activeRadio = radios.comActive ? radios.com : radios.nav;
  Radio & inactiveRadio = radios.comActive ? radios.nav : radios.com;
  if (freqSwapBtn.pop())
  {
    activeRadio.swapFreq();
    drawDirty = true;
  }

  drawDirty |= activeRadio.update();
  drawDirty |= inactiveRadio.update(); // inputs have already been popped, but can now react to serial events / setting changes
  updateSettings();
  redraw();
}

void ComNav::show()
{
  if (!exchange(visible, true))
  {
    drawDirty = true;
    drawCursor();
  }
}

void ComNav::hide()
{
  if (exchange(visible, false))
  {
    display.sendSync(Display::Command::clear);
  }
}

void ComNav::redraw() const
{
  if (!visible) return;
  if (display.isBusy()) return;
  if (!drawDirty) return;

  char displayStr [Display::CHAR_WIDTH * Display::CHAR_HEIGHT];
  for (uint8_t i = 0; i < sizeof(displayStr); ++i) displayStr[i] = ' ';

  /**
   * @param col The column to write to on the screen
   * @param row The row to write to on the screen
   * @returns the index into displayStr for that position, accounting for cursor position
   */
  auto const indexFor = [this](uint8_t col, uint8_t row) -> int
  {
    pair<uint8_t> cursor = cursorPos();

    int loc = col - cursor.first;
    loc += (row - cursor.second) * Display::CHAR_WIDTH;
    if (loc < 0) loc += sizeof(displayStr);

    return loc;
  };

  const bool freqLongPrecision = settings.getRadioChannelSpacing() == Settings::RadioSpacing::EU;
  String s = radios.com.getActiveFreq().toString(freqLongPrecision);
  for (uint8_t i = 0; i < s.length(); ++i) displayStr[indexFor(i, 0)] = s[i];

  s = radios.com.getStandbyFreq().toString(freqLongPrecision);
  for (uint8_t i = 0; i < s.length(); ++i) displayStr[indexFor(i, 1)] = s[i];

  s = radios.nav.getActiveFreq().toString(false);
  for (uint8_t i = 0; i < s.length(); ++i) displayStr[indexFor(Display::CHAR_WIDTH - s.length() + i, 0)] = s[i];

  s = radios.nav.getStandbyFreq().toString(false);
  for (uint8_t i = 0; i < s.length(); ++i) displayStr[indexFor(Display::CHAR_WIDTH - s.length() + i, 1)] = s[i];

  static_assert(sizeof(displayStr) <= 32, "displayStr exceeds display's I2C limits");
  drawDirty = !display.printMaybe(displayStr, sizeof(displayStr));
}

void ComNav::updateSettings()
{
  if (::update(settingsV, settings.getVersion()))
  {
    radios.com.setChannelSpacing(static_cast<uint32_t>(settings.getRadioChannelSpacing()));
    drawCursor();
    drawDirty = true;
  }
}

void ComNav::drawCursor()
{
  if (visible)
  {
    auto const [ col, row ] = cursorPos();
    display.setCursorPosSync(col, row);
  }
}

pair<uint8_t, uint8_t> ComNav::cursorPos() const
{
  if (radios.comActive) return { 3, 1 };
  return { 13, 1 };
}