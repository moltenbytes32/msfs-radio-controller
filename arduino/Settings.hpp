#pragma once

#include "Display.hpp"

#include <Arduino.h>

/** The data model for this program's runtime settings */
class Settings
{
public:
  using Version_T = uint16_t;

  /** The valid Radio Spacings in hHz */
  enum class RadioSpacing : uint32_t
  {
    US =  25000,
    EU =   6250,
  };

  /** Default settings */
  Settings();

  /** @param spacing  The channel spacing in hHz to use */
  void setRadioChannelSpacing(RadioSpacing spacing);
  /** @returns the radio channel spacing */
  RadioSpacing getRadioChannelSpacing() const;

  /** @returns the current version number (incremented every change; starts at 1) */
  Version_T getVersion() const;

  /** Sets power on (true) or off (false )*/
  void setPowerOn(bool powerState);
  /** @returns true iff power is currently on */
  bool isPowerOn() const;

  /** Sets the stored display color */
  void setDisplayColor(DisplayColor color);
  /** Returns the stored display color */
  DisplayColor getDisplayColor() const;

private:
  /** The radio channel spacing */
  RadioSpacing radioChannelSpace;
  /** True iff power is currently on */
  bool powerOn;
  /** The current display color */
  DisplayColor displayColor;

  /** The current version number */
  Version_T version;
};
