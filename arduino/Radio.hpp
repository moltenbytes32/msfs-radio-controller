#pragma once

#include "Frequency.hpp"
#include "Rotary.hpp"
class Settings;

#include "include/common/Common.hpp"

#include <Arduino.h>

/**
 * Implements a basic radio, using two rotaries to control the frequency
 */
class Radio
{
public:

  /**
   * @param radioID           The radio ID to emit frequency changes with
   * @param freq              The initial frequency to start on
   * @param wholeRotary       The rotary controlling the whole part of freq
   * @param fracRotary        The rotary controlling the fractional part of freq
   * @param channelSpacing_Hz The channel spacing to use (hz)
   */
  Radio(RadioID radioID, Frequency const & freq,
        Rotary & wholeRotary, Rotary & fracRotary,
        uint32_t channelSpacing_Hz);

  /** Returns the current frequency */
  Frequency const & getActiveFreq() const;
  /** Returns the current standby frequency */
  Frequency const & getStandbyFreq() const;

  /** Swaps the active and standby frequencies */
  void swapFreq();

  /** 
   * Updates the radio
   * @returns true iff the frequency has changed
   */
  bool update();

  /** @param spacing_Hz The new channel spacing in Hz */
  void setChannelSpacing(uint32_t spacing_Hz);

private:
  /** The radio ID to emit frequency changes with */
  RadioID radioID;
  /** The frequency this radio is on, and the one on standby*/
  struct { Frequency active, standby; } freq;
  /** The rotaries controlling the whole and fractional parts of the frequency */
  Rotary & wholeRotary, & fracRotary;
  /** The current channel spacing */
  uint32_t channelSpacing_Hz;
};
