#include "Settings.hpp"

#include <Arduino.h>

Settings::Settings()
  :radioChannelSpace(RadioSpacing::US)
  ,version(1)
  ,powerOn(true)
  ,displayColor({ 255, 255, 255 })
{

}

void Settings::setRadioChannelSpacing(RadioSpacing spacing)
{
  radioChannelSpace = spacing;
  ++version;
}

Settings::RadioSpacing Settings::getRadioChannelSpacing() const
{
  return radioChannelSpace;
}

uint16_t Settings::getVersion() const
{
  return version;
}

void Settings::setPowerOn(bool state)
{
  if (state != powerOn)
  {
    powerOn = state;
    ++version;
  }
}

bool Settings:: isPowerOn() const
{
  return powerOn;
}

void Settings::setDisplayColor(DisplayColor color)
{
  displayColor = color;
  ++version;
}

DisplayColor Settings::getDisplayColor() const
{
  return displayColor;
}