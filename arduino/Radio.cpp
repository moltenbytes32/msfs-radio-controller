#include "Radio.hpp"
#include "SerLink.hpp"
#include "Settings.hpp"

#include "utility.hpp"

#include <Arduino.h>

Radio::Radio(RadioID radioID_, Frequency const & initialFreq,
             Rotary & w, Rotary & f,
             uint32_t channelSpacing_)
:radioID(radioID_)
,freq({
  initialFreq,
  initialFreq,
})
,wholeRotary(w)
,fracRotary(f)
,channelSpacing_Hz(channelSpacing_)
{

}

Frequency const & Radio::getActiveFreq() const
{
  return freq.active;
}

Frequency const & Radio::getStandbyFreq() const
{
  return freq.standby;
}

void Radio::swapFreq()
{
  freq.active.swap(freq.standby);

  radioID.active = true;
  serLink.emitFreq(freq.active.getFull_Hz(), radioID);
  radioID.active = false;
  serLink.emitFreq(freq.standby.getFull_Hz(), radioID);
}

bool Radio::update()
{
  bool serialChange = false;
  bool inputChange = false;

  radioID.active = true;
  if (uint32_t newFreq = serLink.popNewRadioFreq(radioID))
  {
    serialChange = true;
    freq.active.setFull_Hz(newFreq);
  }
  radioID.active = false;
  if (uint32_t newFreq = serLink.popNewRadioFreq(radioID))
  {
    serialChange = true;
    freq.standby.setFull_Hz(newFreq);
  }

  if (int8_t d = wholeRotary.pop())
  {
    inputChange = true;
    freq.standby.offset_MHz(d);
  }

  if (int8_t d = fracRotary.pop())
  {
    inputChange = true;
    freq.standby.offset_Hz(d * channelSpacing_Hz);
  }

  if (inputChange)
  {
    radioID.active = false;
    serLink.emitFreq(freq.standby.getFull_Hz(), radioID);
  }

  return serialChange || inputChange;
}

void Radio::setChannelSpacing(uint32_t spacing_Hz)
{
  channelSpacing_Hz = spacing_Hz;
  
  if (int16_t diff = freq.standby.get_Hz() % channelSpacing_Hz)
  {
    freq.standby.offset_Hz(-diff);
  }
  if (int16_t diff = freq.active.get_Hz() % channelSpacing_Hz)
  {
    freq.active.offset_Hz(-diff);
  }
}
