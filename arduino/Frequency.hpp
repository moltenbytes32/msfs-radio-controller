#pragma once

#include <Arduino.h>

/**
 * Frequencies are represented as fixed-point values in Hz.
 */
class Frequency
{
public:
  
  /**
   * @param min_mhz the lower bound of this radio
   * @param max_mhz the upper bound of this radio (inclusive)
   */
  Frequency(uint8_t min_mhz, uint8_t max_mhz);

  /**
   * @param min_mhz the lower bound of this radio
   * @param max_mhz the upper bound of this radio (inclusive)
   * @param mhz the whole number part of the frequency to create
   * @param hz the fractional part of the frequency to create
   */
  Frequency(uint8_t min_mhz, uint8_t max_mhz, uint8_t mhz, uint32_t hHz);

  /**
   * Converts this Frequency to a printable string
   * @param longPrecision If true, outputs 7 characters (3 decimals)
   *                      Otherwise, outputs 6 characters (2 decimals)
   */
  String toString(bool longPrecision) const;

  /**
   * Sets the mhz part of the frequency
   * @note OOB values are clamped.
   */
  void set_MHz(uint8_t val_MHz);
  /** @param offset_mhz the offset, in mhz, to adjust this frequency by */
  void offset_MHz(int8_t offset_MHz);
  /** @returns the mHz portion of this frequency */
  uint8_t get_MHz() const;

  /**
   * Sets the fractional part of the frequency (in Hz).
   * The MHz portion of the given value will be ignored.
   * @note OOB values are wrapped.
   */
  void set_Hz(uint32_t val_Hz);
  /**
   * @param offset_Hz the offset, in Hz, to adjust this frequency by
   * The MHz portion will be ignored
   */
  void offset_Hz(int32_t offset_Hz);
  /** @returns the Hz portion of this frequency */
  uint32_t get_Hz() const;

  /** Sets the entire frequency via set_MHz and set_Hz */
  void setFull_Hz(uint32_t val_Hz);
  /** @returns the entire frequency, in Hz */
  uint32_t getFull_Hz() const;

  /** @param other The frequency to swap with this one */
  void swap(Frequency & other);

private:
  /** The upper and lower bounds this frequency supports */
  struct { uint8_t lower_MHz, upper_MHz; } bounds;
  /** The frequency in Hz */
  uint32_t freq_Hz;
};
