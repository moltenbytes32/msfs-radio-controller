#include "SettingsView.hpp"

#include "Display.hpp"
#include "Settings.hpp"
#include "Rotary.hpp"

#include "include/common/Version.hpp"
#include "utility.hpp"

#include <Arduino.h>

SettingsView::SettingsView(Display & display_, Settings & settings_,
                           Rotary & menuRotary_, Rotary & valRotary_)
  :display(display_)
  ,settings(settings_)
  ,menuRotary(menuRotary_)
  ,valRotary(valRotary_)
  ,currentPage({ Page::Begin })
  ,displayColor({ 0xff, 0xff, 0xff })
  ,visible(false)
  ,dirty(true)
{
  
}

void SettingsView::show()
{
  if (!exchange(visible, true))
  {
    dirty = true;
  }
}

void SettingsView::hide()
{
  if (exchange(visible, false)) 
  {
    display.sendSync(Display::Command::clear);
  }
}

void SettingsView::update()
{
  if (!visible) return;

  if (int8_t dir = menuRotary.pop())
  {
    if (dir == 1) ++currentPage;
    if (dir == -1) --currentPage;
    dirty = true;
  }

  if (int8_t dir = valRotary.pop())
  {
    switch(currentPage.val)
    {
      case Page::RadioSpacing:
        settings.setRadioChannelSpacing(
          (settings.getRadioChannelSpacing() == Settings::RadioSpacing::US)
            ? Settings::RadioSpacing::EU
            : Settings::RadioSpacing::US
        );
        break;
      
      case Page::BackgroundRed:
        if (dir > 0) displayColor.red += 0x1;
        if (dir < 0) displayColor.red -= 0x1;
        displayColor.red &= 0x0f;
        displayColor.red |= displayColor.red << 4;
        display.setBacklightColorSync(displayColor);
        settings.setDisplayColor(displayColor);
        break;
      
      case Page::BackgroundGreen:
        if (dir > 0) displayColor.green += 0x1;
        if (dir < 0) displayColor.green -= 0x1;
        displayColor.green &= 0x0f;
        displayColor.green |= displayColor.green << 4;
        display.setBacklightColorSync(displayColor);
        settings.setDisplayColor(displayColor);
        break;
      
      case Page::BackgroundBlue:
        if (dir > 0) displayColor.blue += 0x1;
        if (dir < 0) displayColor.blue -= 0x1;
        displayColor.blue &= 0x0f;
        displayColor.blue |= displayColor.blue << 4;
        display.setBacklightColorSync(displayColor);
        settings.setDisplayColor(displayColor);
        break;
    }
    dirty = true;
  }

  redraw();
}

void SettingsView::redraw() const
{
  if (!visible) return;
  if (!exchange(dirty, false)) return;

  display.sendSync(Display::Command::clear);

  switch(currentPage.val)
  {
    case Page::RadioSpacing:
      display.printSync("Channel Width   ");
      display.printSync(
        settings.getRadioChannelSpacing() == Settings::RadioSpacing::US
          ? "25kHz"
          : "6.25kHz"
      );
      display.setCursorPosSync(0, 1);
      break;
    
    case Page::BackgroundRed:
      {
        display.printSync("Display Color   ");
        char msg [16] = { '\0' };
        sprintf(msg, "Red: 0x%.2x", displayColor.red);
        display.printSync(msg);
      }
      break;
    
    case Page::BackgroundGreen:
      {
        display.printSync("Display Color   ");
        char msg [16] = { '\0' };
        sprintf(msg, "Green: 0x%.2x", displayColor.green);
        display.printSync(msg);
      }
      break;
    
    case Page::BackgroundBlue:
      {
        display.printSync("Display Color   ");
        char msg [16] = { '\0' };
        sprintf(msg, "Blue: 0x%.2x", displayColor.blue);
        display.printSync(msg);
      }
      break;

    case Page::Version:
      display.printSync(String("Version ") + versionStr);
  }
}

SettingsView::Page & SettingsView::Page::operator++()
{
  val = static_cast<Value>(static_cast<uint8_t>(val) + 1);
  if (val == End) val = Begin;
  return *this;
}

SettingsView::Page & SettingsView::Page::operator--()
{
  if (val == Begin) val = End;
  val = static_cast<Value>(static_cast<uint8_t>(val) - 1);
  return *this;
}