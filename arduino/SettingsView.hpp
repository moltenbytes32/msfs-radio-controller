#pragma once

#include "Display.hpp"

class Settings;
class Rotary;

#include <Arduino.h>

/**
 * Displays and manages changing radio and display settings
 */
class SettingsView
{
public:

  /**
   * Creates the new settings screen
   * @param display   The display to render onto
   * @param settings  The settings to display and edit
   * @param menuRotary The rotary to read menu navigation from
   * @param valRotary The rotary to read value edits from
   */
  SettingsView(Display & display, Settings & settings,
               Rotary & menuRotary, Rotary & valRotary);

  /** Call to start drawing the view */
  void show();
  /** Call to hide the view */
  void hide();

  /** Call to update the display */
  void update();

  /** Repaints the display if necessary */
  void redraw() const;

private:
  /** The currently displayed page */
  struct Page
  {
    /** The options and value a page can have*/
    enum Value : uint8_t
    {
      Begin = 0,
      RadioSpacing = 0,
      BackgroundRed,
      BackgroundGreen,
      BackgroundBlue,
      Version,
      End,
    } val;

    /** Go to the next page, with wrapping */
    Page & operator++();
    /** Go back a page, with wrapping */
    Page & operator--();
  };

  /** The display to use */
  Display & display;
  /** The settings to modify */
  Settings & settings;
  /** The rotary to take menu nav input from */
  Rotary & menuRotary;
  /** The rotary to take value input from */
  Rotary & valRotary;
  /** The current page being displayed */
  Page currentPage;

  /** The current background colors */
  DisplayColor displayColor;

  /** True if we're being displayed */
  bool visible;
  /** True if we need to repaint the display */
  mutable bool dirty;
};
