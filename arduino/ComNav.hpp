#pragma once

class Button;
class Display;
class Rotary;
#include "Radio.hpp"
#include "Settings.hpp"
#include "utility.hpp"

#include <Arduino.h>

/**
 * Controls two radios -- a com and a nav
 */
class ComNav
{
public:
  /**
   * Basic initialization
   * @param radioID  The radio ID to use (only uses index)
   * @param settings The radio settings to use
   * @param display The display to draw on
   * @param whole The rotary that should control the whole frequency
   * @param frac  The rotary that should control the fractional frequency
   * @param radioSwap The button to use for com <-> nav focus swapping
   * @param freqSwap The button to use for active <-> stdby freq swapping
   */
  ComNav(RadioID radioID, Display & display, Settings const & settings,
         Rotary & whole, Rotary & frac, Button & radioSwap, Button & freqSwap);

  /** Updates the radios */
  void update();

  /** Shows the radios on the display */
  void show();
  /** Hides the radios on the display */
  void hide();

  /** Redraws the display if necessary */
  void redraw() const;

private:
  /** Updates settings */
  void updateSettings();
  /** Sets the cursor position on the display */
  void drawCursor();
  /** @returns the currently used cursor position (col, row) */
  pair<uint8_t, uint8_t> cursorPos() const;

  /** The display to output on */
  Display & display;
  /** The rotary that controls the whole frequency */
  Rotary & wholeRotary;
  /** The rotary that controls the fractional frequency */
  Rotary & fracRotary;
  /** The radio settings currently in use */
  Settings const & settings;
  /** The last version of settings we've reacted to */
  Settings::Version_T settingsV;
  /** The button to swap the focused radio */
  Button & radioSwapBtn;
  /** The button to swap the focused radio's frequencies */
  Button & freqSwapBtn;

  /** The comms and nav radios */
  struct
  { 
    Radio com, nav;
    bool comActive;
  } radios;

  /** True if we're currently being displayed */
  bool visible;
  /** True if the display needs to be updated */
  mutable bool drawDirty;
};
