#pragma once

class Breakpoint
{
public:
  inline Breakpoint(String const & beforeMsg = "", String const & afterMsg = "")
  {
    if (beforeMsg) Serial.println(beforeMsg.c_str());
    while (Serial.available() == 0);
    while (Serial.available() != 0) Serial.read();
    if (afterMsg) Serial.println(afterMsg.c_str());
  }
};
