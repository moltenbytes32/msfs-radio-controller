#pragma once

#include <Arduino.h>

/**
 * Handles the logic for registering button presses
 */
class Button
{
public:

  /**
   * @param pin The pin to listen on
   * @param pullup True if the pin is pulled-high (false for pulled-low)
   */
  Button(uint8_t pin, bool pullup);

  /** Not copyable because it represents hardware */
  Button(Button const &) = delete;
  /** Not copyable because it represents hardware */
  Button & operator=(Button const &) = delete;

  /** Not movable because it needs to live in the main module */
  Button(Button &&) = delete;
  /** Not movable because it needs to live in the main module */
  Button & operator=(Button &&) = delete;

  /** Hardware setup */
  void setup();

  /**
   * Called at the start of a new frame
   * Polls the hardware and stores the state for this frame
   */
  void poll();

  /** @returns true if this button has been activated for this frame */
  bool peek() const;
  /** @returns true if this button has been activated for this frame, and also resets the state */
  bool pop();

  /** @returns the duration of the current press, or 0 */
  unsigned long pressDuration() const;
  /**
   * Invalidates the current press (if there is one), such that
   * - pressDuration() will return 0 until the next press
   * - the release will not be registered
   */
  void cancelPress();

private:
  /** The pin to listen on */
  uint8_t pin;
  /** The active state of the pin */
  bool const isPullup;
  /** The time that this press started in millis() */
  unsigned long thisPressTime_ms;
  /** The state for this frame */
  bool state;
  /** True if the current press has been invalidated */
  bool invalidatePress;
};
