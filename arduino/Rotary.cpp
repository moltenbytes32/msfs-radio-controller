#include "Rotary.hpp"
#include "utility.hpp"

#include <Arduino.h>

uint8_t Rotary::count = 0;

Rotary::Rotary(uint8_t major, uint8_t minor)
:pins({ .major = major, .minor = minor})
// lastHwState gets initialized in setup()
,inputState(0)
{
  ++count;
}

void Rotary::setup()
{
  pinMode(pins.major, INPUT_PULLUP);
  pinMode(pins.minor, INPUT_PULLUP);
  
  lastHwState.major = digitalRead(pins.major);
  lastHwState.minor = digitalRead(pins.minor);
  lastHwState.latch = false;
}

void Rotary::poll()
{
  bool major = digitalRead(pins.major);
  bool minor = digitalRead(pins.minor);

  auto saveState = defer([this, major, minor]()
  { 
    lastHwState.major = major;
    lastHwState.minor = minor;
  });

  /**
   * Record an input if only one pin differs
   * The subtlty here is that if both pins differ, we missed an input,
   *   and we simply discard it instead of potentially misinterpreting it.
   */
  if ((lastHwState.major == major) != (lastHwState.minor == minor))
  {
    if (!exchange(lastHwState.latch, true))
    {
      if (lastHwState.major != major) inputState = 1;
      else inputState = -1;
      return;
    }
  }
  else if (major == minor) lastHwState.latch = false;
  inputState = 0;
}

int8_t Rotary::peek() const
{
  return inputState;
}

int8_t Rotary::pop()
{
  return exchange<int8_t>(inputState, 0);
}