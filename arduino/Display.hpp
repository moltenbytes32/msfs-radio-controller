#pragma once

#include <Arduino.h>

struct DisplayColor
{
  uint8_t red, green, blue;
};

/**
 * Manages a Sparkfun SerLCD display
 * https://www.sparkfun.com/products/16397
 * 
 * Unlike the SparkFun library though, this uses "cleaner" (newer) C++, and
 *   it also features running a single command asynchronious.
 * 
 * Methods ___Sync are synchronious when a command is already pending.
 * Methods ___Maybe will immediately fail when a command is already pending.
 *   - All *Maybe commands are strictly time based, so loops strictly on their return
 *     values carry strong forward progress guarantees.
 *   - They return true on success, and false on failure
 */
class Display
{
public:

  /** The different control parameters the display accepts */
  struct Control
  {
    /** true to blink the cursor */
    uint8_t blink: 1;
    /** true to show the cursor */
    uint8_t cursor: 1;
    /** true to turn the display on */
    uint8_t power: 1;

    /** Converts to a command byte for transmission */
    operator uint8_t() const;
  };

  /** The different ways the screen will react to new entries */
  struct EntryMode
  {
    /** True to disable line wrapping, and horzontally scroll with text */
    uint8_t shiftInc: 1;
    /** True for left-to-right text */
    uint8_t entryLeft: 1;

    /** Converts to a command byte for transmission */
    operator uint8_t() const;
  };

  /** The different commands that the display accepts */
  enum class Command : uint8_t
  {
    width20 = 0x03,
    width16 = 0x04,
    lines4 = 0x05,
    lines2 = 0x06,
    lines1 = 0x07,
    /** Enables the splash screen */
    enableSplash = 0x30,
    /** Disables the splash screen */
    disableSplash = 0x31,
    /** Saves current text as splash */
    saveSplash = 0x0a,
    /** Displays the screen's firmware version */
    dumpVersion = 0x2c,
    /** Clears the screen */
    clear = 0x2d,
    /** Shows setting messages (ie: Contrast: 5) */
    showSysMsg = 0x2e,
    /** Hides setting messages (ie: Contrast: 5) */
    hideSysMsg = 0x2f,
  };

  /** Allocates resources */
  Display(uint8_t i2cAddr);

  /** Prepares the display for use with I2C */
  void setup();

  /** Prints the given string to the display */
  void printSync(String const & str);
  [[nodiscard]] bool printMaybe(String const & str);

  /**
   * @param str The pascal-style character array
   * @param len The length of str
   */
  void printSync(char const str[], uint8_t len);
  [[nodiscard]] bool printMaybe(char const str[], uint8_t len);

  /** Sends the given control parameter to the display */
  void sendSync(Control control);
  [[nodiscard]] bool sendMaybe(Control control);

  /** Returns the last successfully sent control */
  Control const & getControl() const;

  /** Sends the given entry model to the display */
  void sendSync(EntryMode entry);
  [[nodiscard]] bool sendMaybe(EntryMode entry);

  /** Sends the given command to the display */
  void sendSync(Command cmd);
  [[nodiscard]] bool sendMaybe(Command cmd);

  /** Sets the cursor's location */
  void setCursorPosSync(uint8_t col, uint8_t row);
  [[nodiscard]] bool setCursorPosMaybe(uint8_t col, uint8_t row);

  /** Sets the cursor's visibility */
  void showCursorSync(bool show);
  [[nodiscard]] bool showCursorMaybe(bool show);

  /** Sets the color of the backlight */
  void setBacklightColorSync(DisplayColor color);

  /** Returns the width of the display (in chars) */
  uint8_t charWidth() const;
  static constexpr uint8_t CHAR_WIDTH = 16;
  
  /** Returns the height of the display (in chars) */
  uint8_t charHeight() const;
  static constexpr uint8_t CHAR_HEIGHT = 2;

  /** True if the display can currently accept commands */
  bool isBusy() const;

private:
  /** Sends the given special command */
  [[nodiscard]] bool sendSpecialMaybe(uint8_t command);

  /** Waits until a command can be sent */
  void flush() const;

  /** The current display controls */
  Control currentControl;

  /** The next time (in millis()) that a command can be sent */
  unsigned long nextCmdTime;
  /** The i2c address in use */
  uint8_t i2cAddr;
};
