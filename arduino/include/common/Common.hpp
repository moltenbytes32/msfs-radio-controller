#pragma once


#ifdef _WIN32
#include <cstdint>
#include <functional>
#define uint8_t std::uint8_t  
#define uint16_t std::uint16_t
#define uint32_t std::uint32_t
#else
#include <Arduino.h>
#endif

/** The structure that describes a radio ID */
struct RadioID
{
  uint8_t active : 1; // true for active, false for standby
  uint8_t com : 1;    // true for com, false for nav
  uint8_t index : 1;  // zero-indexed radio

  inline int operator<(RadioID const & other) const
  {
    return asByte() < other.asByte();
  }

  uint8_t asByte() const
  {
    static_assert(sizeof(RadioID) == sizeof(uint8_t));
    return *reinterpret_cast<uint8_t const *>(this);
  }
};

#ifdef _WIN32

namespace std
{
  template<>
  struct hash<RadioID>
  {
    size_t operator()(RadioID id) const
    {
      return id.asByte();
    }
  };
}

#undef uint8_t  
#undef uint16_t
#undef uint32_t
#endif
