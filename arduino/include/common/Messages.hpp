#pragma once

#include "./Common.hpp"

#ifdef _WIN32
#include <cstdint>
#define uint8_t std::uint8_t  
#define uint16_t std::uint16_t
#define uint32_t std::uint32_t
#else
#include <Arduino.h>
#endif

namespace Messages
{
  /** The message header structure */
  struct Header
  {
    static constexpr uint8_t MAGIC_VAL = 0xf0;
    uint8_t magic = 0;

    RadioID radio;

    enum class MessageIDs : uint8_t
    {
      BEGIN = 1,
      SetChannelSpacing = 1,
      SetFrequency = 2,
      Power = 3,
      Debug = 4,
      END,
    } messageID;

    /** Validity check */
    inline operator bool() const
    {
      return magic == MAGIC_VAL && (messageID >= MessageIDs::BEGIN && messageID < MessageIDs::END);
    }
  };

  /** The channel spacing message body */
  struct ChannelSpaceBody
  {
    uint32_t spacing_Hz;
  };

  /** The frequency set message body */
  struct FreqencyBody
  {
    uint32_t freq_Hz;
  };

  /** Turns power on or off */
  struct PowerBody
  {
    /** Nonzero for on, zero for off */
    uint8_t on;
  };

  /** A debug message is an array of characters, terminated by a null */
  struct DebugBody { };
}

#ifdef _WIN32
#undef uint8_t
#undef uint16_t
#undef uint32_t
#endif
