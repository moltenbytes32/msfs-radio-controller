#include "Display.hpp"
#include "utility.hpp"

#include <Arduino.h>
#include <Wire.h>

#define CMD_TIME_GUARD if (millis() < nextCmdTime) return false;

Display::Control::operator uint8_t() const
{
  return *reinterpret_cast<uint8_t const*>(this) | 0x08;
}

Display::EntryMode::operator uint8_t() const
{
  return *reinterpret_cast<uint8_t const*>(this) | 0x04;
}

Display::Display(uint8_t i2cAddr_)
  :i2cAddr(i2cAddr_)
  ,nextCmdTime(0)
{

}

void Display::setup()
{
  Wire.begin();
  sendSync(Command::hideSysMsg);

  currentControl = { .blink = true, .cursor = true, .power = true };
  sendSync(currentControl);

  EntryMode e = { .shiftInc = false, .entryLeft = true };
  sendSync(e);

  sendSync(Command::clear);
}

void Display::printSync(String const & str)
{
  flush();
  while (!printMaybe(str));
}

bool Display::printMaybe(String const & str)
{
  return printMaybe(str.c_str(), str.length());
}

void Display::printSync(char const str[], uint8_t len)
{
  flush();
  while (!printMaybe(str, len));
}

bool Display::printMaybe(char const str[], uint8_t len)
{
  CMD_TIME_GUARD;

  Wire.beginTransmission(i2cAddr);
  Wire.write(str, len);
  Wire.endTransmission();

  nextCmdTime = millis() + 10;
  return true;
}

void Display::sendSync(Control control)
{
  flush();
  while (!sendMaybe(control));
}

bool Display::sendMaybe(Control control)
{
  if (sendSpecialMaybe(control))
  {
    currentControl = control;
    return true;
  }
  return false;
}

Display::Control const & Display::getControl() const
{
  return currentControl;
}

void Display::sendSync(EntryMode entry)
{
  flush();
  while (!sendMaybe(entry));
}

bool Display::sendMaybe(EntryMode entry)
{
  return sendSpecialMaybe(entry);
}

void Display::sendSync(Command cmd)
{
  flush();
  while (!sendMaybe(cmd));
}

bool Display::sendMaybe(Command cmd)
{
  CMD_TIME_GUARD;

  Wire.beginTransmission(i2cAddr);
  Wire.write(0x7c);
  Wire.write(static_cast<uint8_t>(cmd));
  Wire.endTransmission();
  
  nextCmdTime = millis() + 10;
  return true;
}

void Display::setCursorPosSync(uint8_t col, uint8_t row)
{
  flush();
  while (!setCursorPosMaybe(col, row));
}

bool Display::setCursorPosMaybe(uint8_t col, uint8_t row)
{
  uint8_t row_offsets [] = { 0x00, 0x40 };  // stolen from SparkFun's library
  row = clamp<uint8_t>(row, 0, 1);
  col = clamp<uint8_t>(col, 0, 15);

  return sendSpecialMaybe(0x80 | (col + row_offsets[row]));
}

void Display::showCursorSync(bool show)
{
  flush();
  while (!showCursorMaybe(show));
}

bool Display::showCursorMaybe(bool show)
{
  Control c = currentControl;
  c.cursor = show;
  return sendMaybe(c);
}

void Display::setBacklightColorSync(DisplayColor color)
{
  sendSync(static_cast<Command>(0x2b));
  
  Wire.beginTransmission(i2cAddr);
  Wire.write(color.red);
  Wire.write(color.green);
  Wire.write(color.blue);
  Wire.endTransmission();
  
  nextCmdTime = millis() + 10;
}

uint8_t Display::charWidth() const
{
  return CHAR_WIDTH;
}

uint8_t Display::charHeight() const
{
  return CHAR_HEIGHT;
}

bool Display::isBusy() const
{
  return millis() < nextCmdTime;
}

bool Display::sendSpecialMaybe(uint8_t command)
{
  CMD_TIME_GUARD;
  
  Wire.beginTransmission(i2cAddr);
  Wire.write(0xfe);
  Wire.write(command);
  Wire.endTransmission();
  
  nextCmdTime = millis() + 50;
  return true;
}

void Display::flush() const
{
  if (isBusy()) delay(nextCmdTime - millis());
}