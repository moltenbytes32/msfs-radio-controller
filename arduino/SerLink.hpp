#pragma once

#include "include/common/Messages.hpp"
#include "utility.hpp"

#include <Arduino.h>

/**
 * Manages serial link communications
 */
class SerLink
{
public:

  /** Default Constructor */
  SerLink();

  /**
   * Initializes the serial communications link
   * @param baud  The baud rate to establish
   */
  void setup(long baud);

  /**
   * Pops and returns the latest channel spacing command
   * @returns The frequency channel spacing or 0
   */
  uint32_t popLatestChannelSpacing();
  
  /**
   * Pops and returns the latest new freq
   * @returns The new frequency or 0
   */
  uint32_t popNewRadioFreq(RadioID radio);

  /** Returns the current power state */
  optional<bool> popPowerState();

  /** Processes waiting messages */
  void poll();

  /**
   * Emits the frequency for the given radio
   * @param freq    The frequency
   * @param radio   The radio to emit the given frequency on
   */
  void emitFreq(uint32_t freq_Hz, RadioID radio);

  /** Emits the given debug message */
  void emitDebug(char const * message) const;

private:
  union
  {
    /** The latest received header */
    Messages::Header lastHeader;

    /** The buffer for reading bytes */
    byte lastHeaderBuffer[sizeof(Messages::Header)];
  };

  /** The latest requested channel spacing */
  uint32_t lastChannelSpacingRequest;

  /** New radio frequencies received over serial (indexes are RadioIDs) */
  uint32_t newFreqs_Hz[8];

  /** The latest received power state */
  optional<bool> powerState;
};

extern SerLink serLink;