#include "SerLink.hpp"

#include "utility.hpp"

#include <Arduino.h>

SerLink::SerLink()
  :lastChannelSpacingRequest(0)
  ,newFreqs_Hz({0, 0, 0, 0})
  ,powerState()
{

}

void SerLink::setup(long baud)
{
  Serial.begin(baud);
}

uint32_t SerLink::popLatestChannelSpacing()
{
  return exchange<uint32_t>(lastChannelSpacingRequest, 0);
}

uint32_t SerLink::popNewRadioFreq(RadioID radio)
{
  uint8_t index = radio.asByte();
  if (index >= countof(newFreqs_Hz)) return 0;
  return exchange<uint32_t>(newFreqs_Hz[index], 0);
}

optional<bool> SerLink::popPowerState()
{
  return exchange(powerState, optional<bool>());
}

void SerLink::poll()
{
  while (!lastHeader && Serial.available())
  {
    for (uint8_t i = 1; i < sizeof(lastHeaderBuffer); ++i)
    {
      lastHeaderBuffer[i-1] = lastHeaderBuffer[i];
    }

    lastHeaderBuffer[sizeof(lastHeaderBuffer)-1] = Serial.read();
  }

  if (lastHeader)
  {
    switch(lastHeader.messageID)
    {
    case Messages::Header::MessageIDs::SetChannelSpacing:
      if (Serial.available() >= sizeof(Messages::ChannelSpaceBody))
      {
        Messages::ChannelSpaceBody body;
        Serial.readBytes(reinterpret_cast<byte*>(&body), sizeof(body));
        lastChannelSpacingRequest = body.spacing_Hz;
        lastHeader = Messages::Header();
      }
      break;

    case Messages::Header::MessageIDs::SetFrequency:
      if (Serial.available() >= sizeof(Messages::FreqencyBody))
      {
        Messages::FreqencyBody body;
        Serial.readBytes(reinterpret_cast<byte*>(&body), sizeof(body));
        uint8_t freqIndex = lastHeader.radio.asByte();
        if (freqIndex < countof(newFreqs_Hz)) newFreqs_Hz[freqIndex] = body.freq_Hz;
        lastHeader = Messages::Header();
      }
      break;

    case Messages::Header::MessageIDs::Power:
      if (Serial.available() >= sizeof(Messages::PowerBody))
      {
        Messages::PowerBody body;
        Serial.readBytes(reinterpret_cast<byte*>(&body), sizeof(body));
        powerState = static_cast<bool>(body.on);
        lastHeader = Messages::Header();
      }
      break;

    default:
      lastHeader = Messages::Header();
    }
  }
}

void SerLink::emitFreq(uint32_t freq_Hz, RadioID radio)
{
  Messages::Header header;
  header.magic = Messages::Header::MAGIC_VAL;
  header.radio = radio;
  header.messageID = Messages::Header::MessageIDs::SetFrequency;
  Serial.write(reinterpret_cast<byte const*>(&header), sizeof(header));
  Serial.write(reinterpret_cast<byte const*>(&freq_Hz), sizeof(freq_Hz));
}

void SerLink::emitDebug(char const * message) const
{
  Messages::Header debugHeader;
  debugHeader.magic = Messages::Header::MAGIC_VAL;
  debugHeader.messageID = Messages::Header::MessageIDs::Debug;
  Serial.write(reinterpret_cast<byte const *>(&debugHeader), sizeof(debugHeader));
  Serial.write(message);
  Serial.write('\0');
  Serial.flush();
}
