#pragma once

#include <new>

/** std::exchange */
template<typename T>
T exchange(T & lhs, T rhs)
{
  T t = lhs;
  lhs = rhs;
  return t;
}

/**
 * @param lhs The lvalue to assign to
 * @param rhs The value to assign to lhs
 * @returns True if lhs was updated (false if lhs == rhs)
 */
template<typename T>
bool update(T & lhs, T rhs)
{
  if (lhs == rhs) return false;
  lhs = rhs;
  return true;
}

/** std::swap */
template<typename T>
void swap(T & lhs, T & rhs)
{
  rhs = exchange(lhs, rhs);
}

/** std::remove_reference */
template< class T > struct remove_reference
  { typedef T type; };
/** std::remove_reference */
template< class T > struct remove_reference<T&>
  { typedef T type; };
/** std::remove_reference */
template< class T > struct remove_reference<T&&>
  { typedef T type; };

/** std::move */
template<typename T>
typename remove_reference<T>::type && move(T && t)
{
  return static_cast<typename remove_reference<T>::type &&>(t);
}

/** std::clamp */
template<typename T>
constexpr T const & clamp(T const & val, T const & min, T const & max)
{
  return val > max ? max :
          val < min ? min :
           val;
}

/** std::pair */
template<typename T1, typename T2 = T1>
struct pair
{
  T1 first;
  T2 second;
};

/**
 * Invokes the predicate once the end of the scope of this object has been reached
 */
template<typename Callback>
class DeferT
{
public:
  DeferT(Callback && c)
  :callback(move(c))
  ,valid(true) 
  { }

  DeferT(DeferT const &) = delete;
  DeferT & operator=(DeferT const &) = delete;

  DeferT(DeferT && other)
  :callback(other.callback)
  ,valid(other.valid)
  {
    other.valid = false;
  };
  
  DeferT & operator=(DeferT && other)
  {
    if (this != &other)
    {
      valid = false;
      this->~DeferT();
      new (this) DeferT(move(other));
    }
  };

  ~DeferT()
  {
    if (exchange(valid, false)) callback();
  }

private:
  Callback callback;
  bool valid;
};

/** Wrapper function for DeferT for type deduction */
template<typename Callback>
DeferT<Callback> defer(Callback && c)
{
  return DeferT<Callback>(move(c));
}

/** Yields the size of a compile-time array */
template<typename T, size_t N>
constexpr size_t countof(T(&)[N])
{
  return N;
}

/** A rudimentary optional type */
template<typename T>
class optional
{
public:
  /** Constructs an optional with no value */
  optional()
  :hasValue(false)
  {
    
  }

  /** Constructs an optional from a value */
  optional(T && val_)
  :hasValue(true)
  ,val(move(val_))
  { }

  /** Can be copied via copying T */
  optional(optional<T> const & other)
  :hasValue(other.hasValue)
  ,val(other.val)
  { }

  /** Copy assignment */
  optional<T> & operator=(optional<T> const & other)
  {
    if (this != &other)
    {
      this->~optional();
      new (this) optional(other);
    }
    return *this;
  }

  /** Move construction via moving T */
  optional(optional<T> && other)
  :hasValue(other.hasValue)
  ,val(move(other.val))
  {
    other.hasValue = false;
  }

  /** Move assignment */
  optional<T> & operator=(optional<T> && other)
  {
    if (this != &other)
    {
      this->~optional();
      new (this) optional(move(other));
    }
    return *this;
  }

  /** Direct value placement */
  optional<T> & operator=(T && val_)
  {
    this->~optional();
    new (this) optional(move(val_));
    return *this;
  }

  /** Destructor */
  ~optional()
  {
    hasValue = false;
  }

  /** True iff a value is currently contained */
  bool has_value() const { return hasValue; }
  /** Synonym for has_value */
  operator bool() const { return has_value(); }

  /**
   * @returns a pointer to the contained value, or nullptr if no value is contained.
   */
  T * get_value() const { return hasValue ? &val : nullptr; }

private:
  bool hasValue;
  T val;
};