#pragma once

#include <Arduino.h>

/**
 * Implements a polled 2 wire rotary encoder
 * 2 wire rotary encoders work as follows:
 * - When the knob is rotated clockwise, the major line transitions before the minor line
 * - When the knob is rotated counterclockwise, the major line transitions after the minor line
 */
class Rotary
{
public:
  static uint8_t count;

  /**
   * Constructs a new rotary class, used for tracking the state of a rotary value
   * @param major The 'a' pin in the rotary pair
   * @param minor The 'b' pin in the rotay pair
   */
  Rotary(uint8_t major, uint8_t minor);

  /** Not copyable because it represents hardware */
  Rotary(Rotary const &) = delete;
  /** Not copyable because it represents hardware */
  Rotary & operator=(Rotary const &) = delete;

  /** Not movable because it needs to live in the main module */
  Rotary(Rotary &&) = delete;
  /** Not movable because it needs to live in the main module */
  Rotary & operator=(Rotary &&) = delete;

  /** Hardware level initialization */
  void setup();

  /**
   * Called at the start of a new frame
   * Polls the hardware and stores the state for this frame
   */
  void poll();

  /** @returns true if this button has been activated for this frame */
  int8_t peek() const;
  /** @returns true if this button has been activated for this frame, and also resets the state */
  int8_t pop();

private:
  /** The major and minor pins in use */
  struct
  {
    uint8_t major, minor;
  } pins;

  /** Last known hardware states */
  struct
  {
    /** The last know major and minor pin states */
    bool major, minor;
    /** Ensures that once change isn't captured multiple times */
    bool latch;
  } lastHwState;

  /** This frame's high-level state */
  int8_t inputState;
};