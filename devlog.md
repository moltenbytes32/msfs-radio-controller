# 2022-01-04
Aaaand I've now spent several hours trying to fix a bug caused by a missing `break` statement. Classic.

For some reason, I've been having issues with the Windows 10 driver sending the power command to the controller. The vast majority of the time, the commands get ignored, but when I send the byte stream from a program like Realterm, things just work.

Welp, here's the broken case statement:

```
// SerLink.cpp:73
    case Messages::Header::MessageIDs::Power:
      if (Serial.available() >= sizeof(Messages::PowerBody))
      {
        Messages::PowerBody body;
        Serial.readBytes(reinterpret_cast<byte*>(&body), sizeof(body));
        powerState = static_cast<bool>(body.on);
        lastHeader = Messages::Header();
      }

    default:
      lastHeader = Messages::Header();
```

You see... When we're waiting for a power message, but don't have enough bytes accumulated to read the body in yet we skip the `if`, but fall through to the `default` label, where we obliterate the header to prepare to read in more data.

I think this also explains yesterday's weirdness, so it seems that `Serial.available()` call isn't broken after all. Sorry Sparkfun, I owe you guys a beer.

On the upside, I've now got a debug message implemented to make future development easier.

# 2022-01-03 (later)
Welp, I found a bug earlier where the radio would only accept a few of the power on-off messages before freezing up entirely. It appears to be a bug with how Sparkfun is processing serial data -- even if there is room in the Serial buffer, if `Serial.available()` returns anything other than 0, new messages are never accepted. They probably didn't want to implement a ring buffer.

Either way, it's been resolved too, and in a relatively elegant manner.

# 2022-01-03
Last night and this morning, I added a nifty little feature that powers the display down when the avionics bus is powered off in game.
Given how straightforward it was, I'm pretty proud of the code.

I also added version numbering because that's generally important.

# 2021-09-26
TODAY WAS THE DAY -- I successfully completed a flight from KMFR -> KOTH on PilotEdge using the radio controller.
To close the gap, I added code that syncs the radio with MSFS if it changes in-sim, so that's a two way connection now. Other than that I fixed an issue with the configuration code for the display. I'm not sure what fixed it tbh, but I'm sure that won't be an issue.

Once the software was complete, I remounted the circuits into the new shell that has been waiting for me since before the trip, and learned a few lessons:
1. 3M strips won't stick well to PLA unless you (wet) sand the PLA
2. The levers on the throttles make twisting the dials a little harder than anticipated (an offset mounting would help quite a bit)
3. Sanding down the large knob ever so slightly helps the fit a lot

It felt really good to fly with though, and it works super well. This is probably going to be it for the first release, but I definitely want to revisit it to incorporate the second set of radios in somehow.

# 2021-09-25
Today was my first day back from the trip, and thankfully the 3.3v replacement board was waiting for me, so I managed to write the interface that sets the radio's channel spacing from MSFS, and I've gotten the radio to push frequencies to MSFS! After a few bug fixes, the icing on the cake for today was implementing setting pages to control the display's rgb colors.

At this point, only one final boss remains: I need to synchronize frequencies the other way so that when frequencies change in the sim, it reflects on the radio controller. That will also have the bonus points of setting the controller to match the sim on boot. Tomorrow's the day.

# 2021-09-19
HOLY SHIT, I finally managed to programmatically set radio frequencies in MSFS.

Things I tried but didn't work

- Directly setting the frequency from SimConnect
  + After trying, I realized that this is set as "read only" and is not writable, so that's on me
- Mapping the WASM event in SimConnect and setting that
  + This should've worked, but didn't
- Directly setting the frequency in WASM via BCD32
  + This should've worked, but didn't
- Directly setting the frequency in WASM via undocumented variable in Decimal HZ
  + This shouldn't have worked, and didn't

What finally worked was mapping the undocumented WASM variable to SimConnect and manipulating that. Which really shouldn't have worked, but did.

Hotwiring the 5v board to become the 3.3v has also proven to be a bit tricker than anticipated. I've used an AVR programmer to re-burn the bootloader for the pro micro, but despite the schematics being identical on Sparkfun's website, the 3.3v variant ships with an 8MHz resonator. I've got work travel this week, so I've ordered some 3.3v boards that will greet me when I get home so I can finally wrap this project up next weekend.

# 2021-09-18
Alright. I've setup a WASM project, and finally gotten it to receive events transmitted from the SimConnect side driver.
Unfortunately, it also cannot set the radio frequency. What it can do though is turn off the NAV lights.
Sometimes it's the small victories.

# 2021-09-17
Welp. Neither `SimConnect_SetDataOnSimObject` nor `SimConnect_TransmitClientEvent` seem to work for changing aircraft data, and I just broke the USB header off the Pro Micro.
Unfortunately, I don't have another 3.3v to replace it with. I do have a 5v that I'm going to hotwire into a 3.3v, but if it doesn't work, I'll probably fry the display too.

# 2021-09-16
I've gotten Windows successfully reading radio data sent over serial from the board. It needs to be a bit cleaner, but it's getting there!
A bit of cleanup, and the WASM mod, and it'll be ready to fly!

# 2021-09-12
Last night's problem was caused by the classic C++ issue that basically resembles the difference between `MyClass & a, b` and `MyClass & a, & b`, where in the former `b` was actually stored by-copy. In hindsight, the copy constructor for hardware backed classes has been deleted to avoid this in the future.

Aside from that, I've gotten 8.33kHz spacing on the radio working, and I've got an integration with MSFS 2020 that can derive radio characteristics from in-game, but I've just now realized that "8.33kHz" spacing in the sim is actually 6.25kHz. Why. What the fuck.

**Microsoft Interrogation Question2** WHY DID YOU LIE TO ME?

Anyway, that's been fixed, and I also realized that NAV spacing should always be at 50kHz spacing.

I also now have the radio driver successfully scanning for and locating the dradio board by monitoring which serial ports exist and looking for a new one getting plugged in.

# 2021-09-11
Yesterday and today I created a settings object and a view for changing it, and made the radio view play nice with it.

I'm still working on refactoring the input system to play super cleanly between them. For some reason, the fractional radial encoder can't be read from inside Radio::udpdate, but it seems to work everywhere else...

# 2021-09-09
Alright, after several days of nothing, I've finally gotten a background WASM script loaded and running, as evident by a breakpoint in VS 2019.
That's it. :disappointed:

**Microsoft Interrogation Question 1** Why do I need to write 2 mods for this??

# 2021-09-06
So I looked into fixing the LCD firmware, but it was gonna cost me money to buy a custom programmer, which would also cost shipping time. The bug was that the cursor wouldn't show in the first column of the second row if it appeared there due to line wrap. So I moved the cursor to show on the decimal points of the radio. Problem solved.

Aside from that, I've cleaned up the codebase a lot, and finished the UI.

On the 3D printing side, I'm almost done with the bottom part of the panel, and then it'll be time to print the full, final case.

I also started working on the MSFS integration piece. That's gonna be a bit of a pain. SimConnect was relatively straightforward, but it's really only good for reading data -- most of MSFS's variables aren't settable from SimConnect. Instead, you have to use WASM. Except that WASM doesn't have the Windows API, and therefore cannot access serial.

So I'm gonna have to take a lead from markrielaart on github and write a SimConnect driver that can talk to the arduino board and send events to a WASM driver that will actually manipulate the radio in game. wtf.

# 2021-09-05
I've spent the past few days finishing up the hardware of the project; at this point I've got the headers on all the boards, as well as a Power+I2C distribution board put together. Additionally, I've got the front plate 3D printed and done, including a custom active / standby swap button.

Unfortunately, the LCD firmware appears to be bugged, with the cursor not wrapping until after a character has been printed on a line, but that's a problem for another day.

# 2021-08-31
Today was pretty simple -- I soldered headers onto the dual rotary knob, and wired it up to the arduino, creating a rotary driver, a frequency class, and a Radio class to bind them together.

The frequency class was a little more interesting than I first though -- I was originally planning on doing 2 ints and just having `whole.frac` kind of notation, but due to the size of `uint8_t`, as well the thought of what would look like to work with that kind of thing, I settled on `MHz.(KHz * 10)` notation, which lets me store `121.5` as `121` and `50`, which works nicely.

# 2021-08-30
Okay, so I just tried to update all of the past commits to use MoltenBytes email instead of my real one.
I backed the repo up, and messed with the filter until I got it working. Then I deleted the working copy and restored from backup to do it for real, only to find the backup empty.

Apparently in powershell `copy-item <folder> <dest>` will create `<dest>` as a folder, but will copy none of the contents because `-recurse` wasn't passed. I guess MS said "well, I copied the folder itself, and that's good enough". Even worse, is that `copy-item` is aliased to `cp` so anyone from a linux system is gonna fall for this.

**Lesson 1** Check your backups before you call them backups.

Anyway, I've reimplemented USB_Buttons as well as USB_Dial for experimenting with the different input types.

# 2021-08-29
I started by setting up the serial interface and arduino development environment under Windows. It's a lot harder than linux (who would've guessed?)
After that, I spent most of the night screaming at USB interfaces trying to get it to show up as a game controller.

In the end though, I made it, and got it working in MSFS